#include <iostream>
#include "Arbol.h"
#include "Grafo.h"
using namespace std;

Arbol::Arbol(){
	
}

// crea nodo
Nodo* Arbol::crear_nodo(int numero, Nodo *padre){
    Nodo *q;
    q = new Nodo;

    q->dato = numero;
    q->izq = NULL;
    q->der = NULL;
    q->padre = padre;

    return q;
}

// Se añada los nodos en las posiciones
void Arbol::insertar_nodo(Nodo *&raiz, int numero, Nodo *padre){
    int nodo_raiz;
    // se da valor a la raiz
    if(raiz == NULL){
        Nodo *tmp;
        tmp = crear_nodo(numero, padre);
        raiz = tmp;
        insertar_nodo(raiz, numero, padre);
    }
    else{
        nodo_raiz = raiz->dato;
        // Si el numero es menor a la raiz se posiciona a la izquierda
        if(numero < nodo_raiz){
            insertar_nodo(raiz->izq, numero, raiz);
        }
        // Si el número es mayor a la raiz se posiciona a la derecha
        if(numero > nodo_raiz){
            insertar_nodo(raiz->der, numero, raiz);
        }
    }
}

// imprime el arbol en inorden
void Arbol::arbol_inorden(Nodo *raiz){
    if(raiz != NULL){
        arbol_inorden(raiz->izq);
        cout << raiz->dato << " - ";
        arbol_inorden(raiz->der);
    }
}

// imprime el arbol en preorden
void Arbol::arbol_preorden(Nodo *raiz){
    if(raiz != NULL){
        cout << raiz->dato << " - ";
        arbol_preorden(raiz->izq);
        arbol_preorden(raiz->der);
    }
}
// imprime el arbol en posorden 
void Arbol::arbol_posorden(Nodo *raiz){
    if(raiz != NULL){
        arbol_posorden(raiz->izq);
        arbol_posorden(raiz->der);
        cout << raiz->dato << " - " ;
    }
}

// crea el grafo
void Arbol::crear_grafo(Nodo *raiz){
  Grafo *grafo = new Grafo(raiz);
}



