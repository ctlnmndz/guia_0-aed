#include <fstream>
#include <iostream>
#include "Programa.h"
#include "Grafo.h"
using namespace std;


/*crea el archivo .txt de salida y
  le agrega contenido del árbol para generar el grafo (imagen).*/

Grafo::Grafo(Nodo *nodo){
	ofstream fp;

	// abre archivo
	fp.open ("grafo.txt");

	fp << "digraph G {" << endl;
	fp << "node [style=filled fillcolor=pink];" << endl;

	recorrer(nodo, fp);

	fp << "}" << endl;

	// cierra archivo
	fp.close();
	
	// genera grafo
	system("dot -Tpng -ografo.png grafo.txt &");

	// visualiza grafo
	system("eog grafo.png &");
}


/*
* recorre en árbol en preorden y agrega datos al archivo.
*/
void Grafo::recorrer(Nodo *p, ofstream &fp){
	if (p != NULL) {
		if (p->izq != NULL) {
			fp <<  p->dato << "->" << p->izq->dato << ";" << endl;
			}
		else {
			string cadena = std::to_string(p->dato) + "i";
			fp << "\"" << cadena << "\"" << " [shape=point];" << endl;
			fp << p->dato << "->" << "\"" << cadena << "\"" << ";" << endl;
		}
		if (p->der != NULL) {
				fp << p->dato << "->" << p->der->dato << ";" << endl;
		}
		else {
			string cadena = std::to_string(p->dato) + "d";
			fp << "\"" <<  cadena << "\"" << " [shape=point];" << endl;
			fp << p->dato << "->" << "\"" << cadena << "\"" << ";" << endl;
		}

		recorrer(p->izq, fp);
		recorrer(p->der, fp);
	}
}
