#include <fstream>
#include <iostream>
using namespace std;

#ifndef GRAFO_H
#define GRAFO_H

class Grafo {
    private:

    public:
     	// constructor
        Grafo(Nodo *nodo);
		// recorre el arbol
        void recorrer(Nodo *p, ofstream &fp);
};
#endif
