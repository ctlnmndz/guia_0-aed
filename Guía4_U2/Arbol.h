#include <iostream>
#include "Programa.h"

#ifndef ARBOL_H
#define ARBOL_H


class Arbol{
    private:

    public:
        Arbol();
        // crea nodo
        Nodo* crear_nodo(int numero, Nodo *padre);
        // agrega nod al arbol
        void insertar_nodo(Nodo *&raiz, int numero, Nodo *padre);
        // imprime en inorden
        void arbol_inorden(Nodo *raiz);
        // imprime en preorden
        void arbol_preorden(Nodo *raiz);
        // imprime en posorden
        void arbol_posorden(Nodo *raiz);
        // crea el grafo
        void crear_grafo(Nodo *raiz);

};
#endif
