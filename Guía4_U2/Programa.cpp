#include <iostream>
#include <fstream>
#include <stdio.h>
#include "Arbol.h"
#include "Grafo.h"

using namespace std;


// funcion que ingresa nodos al arbol
void ingresar(Arbol *arbol, Nodo *&raiz, int repetir[], int numero){
	int contador = 0;
	Nodo *n = NULL;
	// se ve si el numero ya lo han ingresado
	for(int i=0; i<1000; i++){
		// si ya lo ingresaron el contador es 0
		if(repetir[i]==numero){
			contador = 0;
		}
		else{
			// si no se añade a la lista
			repetir[i]= numero;
			contador = 1;
		}
	}
	// si el numero no se encuentra en el arbol se crea el nodo
	if(contador == 1){
        n = arbol->crear_nodo(numero, NULL);
        // si es el primer numero se crea como raiz
   		if(raiz == NULL){
   			raiz = n;
        }
        // si hay mas de un numero en el arbol
       	else{
    		arbol-> insertar_nodo(raiz, numero, NULL);
       	}
    }
    // si el numero ya se encuentra en el arbol
    else{
		cout << "el numero ya se encuentra en el arbol" << endl;
	}
}

// menu
void menu(Arbol *arbol, Nodo *raiz, int repetir[]){
	int opcion=0;
	int numero;
	cout << "ÁRBOL BINARIO" << endl;
	// Ciclo para repetir las opciones del menu
	while (opcion!=6){
		cout << "***********************" << endl;
		cout << "Ingrese una opción:" << endl;
		cout << "1.- Insertar nodo" << endl;
		cout << "2.- Modificar nodo" << endl;
		cout << "3.- Eliminar nodo" << endl;
		cout << "4.- Imprimir nodo" << endl;
		cout << "5.- Mostrar grafo" << endl;
		cout << "6.- Salir" << endl;
		cout << "***********************" << endl;
	    cin >> opcion;
	    
	    // Se agregan nodos al arbol
		if (opcion==1){
			cout << "Ingrese el número a agreagar:" << endl;
			cin >> numero;
			// se llama la funcion para anañir nodos sal arbol
			ingresar(arbol, raiz, repetir, numero);
		}
		// modifica
		else if(opcion == 2){
			cout << "Aún no modifica:" << endl;

		}
		// elimina
		else if(opcion == 3){
			cout << "Aún no elimina:" << endl;

		}
		// opcion para imprimir el arbol
		else if(opcion == 4){
			/* se llaman las funciones que imprimen los orden del arbol*/
			cout << " ****Preorden****" << endl;
            arbol->arbol_preorden(raiz);
            cout << endl;
            cout << "****inorden****" << endl;
            arbol->arbol_inorden(raiz);
            cout << endl;
            cout << "****posorden****" << endl;
            arbol->arbol_posorden(raiz);
            cout << endl;

		}
		// opcion que crea y muestra el grafo
		else if(opcion == 5){
			cout << "Grafo:" << endl;
			arbol->crear_grafo(raiz);

		}
		// opcion para salir del programa
		else if(opcion == 6){
			cout << "Ha salido del programa, xao xoxo" << endl;
		}
		else{
			 cout << "Opción no válida" << endl;		
		}
	}
}


// función principal
int main(void) {
	// se crea el arbol
	Arbol *arbol = new Arbol();
	// se crea el nodo raiz
	Nodo *raiz = NULL;
	// se crea la lista que verificara si los numeros estan repetidos
	int repetir[1000];
	// se lena de 0 la lista
	for(int i=0; i<1000; i++){
		repetir[i] = 0;
	}
	// se llama la funcioon menu
	menu(arbol, raiz, repetir);

  return 0;
}
