# Pilas y Colas Circulares con Arreglos
El programa permite añadir, eliminar y ver una pila. Se debe ingresar la cantidad maxima de la pila

# Empezando
El programa pedirá la cantidad maxima de la pila, luego mostrara el meú donde deberá eleigir una de las opciones (ingresar pila, remover pila, ver pila, salir) donde cada opción realizara y pedirá los datos correspondiente.

# Prerequisitos

- Sistema operativo linux
- Editor de texto
- Lenguaje C++
- Make
Si no lo tiene instalado lo puede hacer con el siguiente comando:
sudo apt install make

# Ejecutando 
Para ejecutar sólo debes abrir una terminal en la carpeta donde se encuentre el programa y ejecutar "make"
Luego de haber ejecutado debes ingresar "./Programa"

El programa pedirá ingresar la cantidad maxima de la pila, ingresando esto se irá directamente al menú donde habrán 4 opciones:
1.- Ingresar pila: donde se evaluara si la pila esta llena o no y si no esta llena pedira el dato de la pila a añadir. luego se ingresará y volvera al menú, pero si esta llena no se añadera nada y volverá al menú.
2.- Remover pila: donde se evaluara si la pila esta vacía o no, si la pila no esta vacía se eliminara el último dato ingresado y volvera al meú. Si la pila esta vacía no se eliminará nada y volverá al menú.
3.- Ver pilas: donde se vera si la pila esta vacia, si esta vacía no se imprime la pila, si no esta vacía mostrara las pilas ingresadas y volverá al menú.
0.- Salir: sale del programa
Si se apreta una opción no valida se imprime "opcion no valida" y volverá a mostrar el menú.

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta y cumpliendo con los prerequisitos.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : lenguaje de programación.
- Geany: editor de texto.

# Autores
Catalina Méndez Cruz
cmendez19@aulumnos.utalca.cl