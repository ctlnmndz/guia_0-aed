using namespace std;

#ifndef PILA_H
#define PILA_H

// Se crea la clase pila.
class Pila{
    private:

        int max = 0;
        int *pilas = NULL;
        int tope = 0;
        bool band;
        int dato;

    public:
        // Constructor de la clase.
        Pila(int max);
        void Pila_llena();
        void push();
        void Pila_vacia();
        void pop();
        void imprimir_pila();
};
#endif
