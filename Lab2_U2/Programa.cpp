#include <iostream>
using namespace std;
#include "Pila.h"

void menu(Pila pila){
	int opcion = 10;
	// ciclo para pedir el manú las veces necesarias hasta que opriman el 0.
	while(opcion != 0){
		cout << "**************MENÚ**************" << endl;
		cout << "Ingrese una opción: " << endl;
		cout << "1. Ingresar una pila" << endl;
		cout << "2. Remover una pila" << endl;
		cout << "3. Ver pilas" << endl;
		cout << "0. Salir" << endl;
		cout << "********************************" << endl;
		cin >> opcion;
		
		// se llama el metodo de añadir pila en la clase pila
		if(opcion == 1){
       		pila.push();

		}
		// Se llama el metodo de eliminar pila en la clase pila
		else if(opcion == 2){
        	pila.pop();

		}
		// Se llama el metodo para imprimir pila de la clase pila
		else if(opcion == 3){
			pila.imprimir_pila();

		}
		// Se sale del programa
		else if(opcion == 0){
			cout << "Ha salido del progama" << endl;
		}

		else{
			cout << "Opción invalida" << endl;
		}
	}
}
int main(){
	
	// Se pide la cantidad maxima de pilas a añadir.
	int max=0;
	cout << "Ingrese la cantidad maxima de pilas: " << endl;
	cin >> max;
	
	//Se llama la clase pila.
	Pila pila = Pila(max);
	
	//Se llama el manú.
	menu(pila);
	return 0;
}
