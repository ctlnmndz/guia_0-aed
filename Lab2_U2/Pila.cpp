#include <iostream>
using namespace std;
#include "Pila.h"

// Constructor de la clase
Pila::Pila(int max){
    this-> max = max;
    this-> tope = tope;
    this-> pilas = new int[max];
    bool band;
    this-> band = band;
    int dato;
    this-> dato = dato;

}

// evalúa si la pila está llena.
void Pila::Pila_llena(){
    // si el tope es igual a la cantidad maxima, la pila esta llena.
    if(this-> tope  ==  this-> max){
        this-> band = true;
    }
    // si no no esta llena.
    else{
        this-> band = false;
    }
}

// agrega elementos a la pila
void Pila::push(){
    // Vemos si la pila esta llena.
    Pila_llena();
    // Si esta llena no se agrega el dato.
    if(this-> band == true){
		cout << "Desbordamiento, Pila llena" << endl;
    // si no esta llena se añade el dato al arreglo pila.   
    }else{
        //Se pide el dato.
        cout << "Ingrese el dato de la pila que desa agregar: ";
        cin >> this-> dato;
        // Se añade.
        this-> pilas[this->tope] = dato;
        // El tope aumenta en 1.
        this -> tope = this-> tope +1;
        cout << "¡Dato agregado con exito!" << endl;
    }
    cout << endl;
}

// evalúa si la pila esta vacía.
void Pila::Pila_vacia(){
	// si el tope es igual a 0, la pila esta vacía.
    if(this-> tope == 0){
        this-> band = true;
 
    }else{
      this-> band = false;
    }
}

// Elimina elementos de la pila.
void Pila::pop(){
	// Se ve si la pila esta vacía.
    Pila_vacia();
    // Si esta vacía no se elimina nada.
    if(this-> band == true){
		cout << "Subdesbordamiento, Pila vacı́a" << endl;
	// Si no esta llena se elimina el dato añadido.
    }else{
		// El tope disminuye en 1.
		this -> tope = this-> tope -1;
		cout << "¡Dato elimidado con exito!" << endl;        
    }
}

// Imprime la pila.
void Pila::imprimir_pila(){
	// se ve si la pila esta vacía.
	Pila_vacia();
	// Si esta vacía no se imprime nada
	if(this-> band == true){
		cout << "No hay datos para mostrar, pila vacia" << endl;
	}
	// Si no esta vacía imprime los datos de la pila
	else{
		cout << "********************************" << endl;
		cout << "Estado actual de la pila: " << endl;
		for(int i=0; i<this->tope ;i++){
    		cout << "|" << this-> pilas[i] << "|" << endl;
		}
 		cout << "********************************" << endl;
 	}
}
