#include <list>
#include <iostream>
using namespace std;
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"


// Función para imprimir los datos ingresados de la proteína.
void print_proteina (list <Proteina> proteinas) {
    
    cout << endl;
    cout << "***** Proteína Ingresada *****" << endl;
   
    // Entra en la lista de proteinas e imprime
    for (Proteina p: proteinas){
        cout << "Proteina: " << p.get_nombre() <<  endl;
        cout << "Id: " << p.get_id() << endl;
        
         for(Cadena cadena: p.get_cadena()){
             cout << "Cadena: " << cadena.get_letra() << endl;
        
             for(Aminoacido aminoacido: cadena.get_aminoacido()){
                 cout << "Numero de aminoacidos: " << aminoacido.get_numero_aa() << endl;
                 cout << "Nombre del aminoacido: " << aminoacido.get_nombre_aa() << endl;
        
                 for(Atomo atomo: aminoacido.get_atomo()){
                     cout << "Numero de atomos: " << atomo.get_numero_a() << endl;
                     cout << "Nombre del atomo: " << atomo.get_nombre_a() << endl;
                     cout << "Coordenada x: " << atomo.get_coordenada().get_x() << endl;
                     cout << "Coordenada y: " << atomo.get_coordenada().get_y() << endl;
                     cout << "Coordenada z: " << atomo.get_coordenada().get_x() << endl;
                 }
             }
        }
    }
}

// Función que pide ingresar los datos de la proteína.
void Ingresar_Proteina (list <Proteina> proteinas) {
    
    string nombre;
    string id;
    
    string letra;
   
    int numero_aa;
    string nombre_aa;
    
    int numero_a;
    string nombre_a;
    
    float x;
    float y;
    float z;
    
    // Se pide la información.
    cout << endl;
    cout << "Nombre de la proteína: " << endl;
    cin >> nombre;
    
    cout << endl;
    cout << "Id de la proteína:     " << endl;
    cin >> id;
    
    cout << endl;
    cout << "Letra de la cadena de la proteína: " << endl;
    cin >> letra;
    
    cout << endl;
    cout << "Número de aminoacidos: " << endl;
    cin >> numero_aa;
    
    cout << endl;
    cout << "Nombre de aminoacidos: " << endl;
    cin >> nombre_aa;
    
    cout << endl;
    cout << "Número de atomo: " << endl;
    cin >> numero_a;
    
    cout << endl;
    cout << "Nombre de atomo: " << endl;
    cin >> nombre_a;
    
    cout << endl;
    cout << "Coordenada x: " << endl;
    cin >> x;
    
    cout << endl;
    cout << "Coordenada Y: " << endl;
    cin >> y;
    
    cout << endl;
    cout << "Coordenada z: " << endl;
    cin >> z;
    
    // Se llaman las clases.
    Proteina p = Proteina(nombre, id);
    Cadena cadena = Cadena(letra);
    Aminoacido aminoacido = Aminoacido(numero_aa, nombre_aa);
    Atomo atomo = Atomo(numero_a, nombre_a);
    Coordenada coordenada = Coordenada(x, y ,z);
    
    // Se sube la informacion a la lista proteína
    atomo.set_coordenada(Coordenada(x,y,z));
    aminoacido.add_atomo(atomo);
    cadena.add_aminoacido(aminoacido);
    p.add_cadena(cadena);
    proteinas.push_back(p);
    print_proteina(proteinas);

}

// Función principal donde esta el menú
int main (void) {
    
    int opcion;
    int a=1;
    
    list <Proteina> proteinas;
    
    while(a == 1){
        cout << endl;
        cout << "***Proteinas***" << endl;
        cout << "Ingrese una opción: " << endl;
        cout << "1.- Añadir proteina" << endl;
        cout << "2.- Salir del programa" << endl;

        cin >> opcion;
        
        if (opcion == 1){
            Ingresar_Proteina(proteinas);
        }
        else{
            cout << "**** Gracias por su visita****" << endl;
            a = 2;
        }
    }
    
    return 0;
}
