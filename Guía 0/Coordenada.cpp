#include <iostream>
using namespace std;
#include "Coordenada.h"

// Constructor
Coordenada::Coordenada(){
    float x = 0;
    float y = 0;
    float z = 0;
}
Coordenada::Coordenada(float x, float y, float z){
    this->x = x;
    this->y = y;
    this->z = z;
}

// Metodos get y set
float Coordenada::get_x(){
    return this->x;
}
void Coordenada::set_x(float x){
    this->x = x;
}

float Coordenada::get_y(){
    return this->y;
}
void Coordenada::set_y(float y){
    this->y = y;
}

float Coordenada::get_z(){
    return this->z;
}
void Coordenada::set_z(float z){
    this->z = z;
}
