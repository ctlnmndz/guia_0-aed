#include <iostream>
#include <list>
using namespace std;
#include "Cadena.h"
#include "Aminoacido.h"


// Constructor 
Cadena::Cadena(string letra){
    this->letra = letra;
    list <string> aminoacido;
}

// Metodo get y set
string Cadena::get_letra(){
    return this->letra;
}

void Cadena::set_letra(string letra){
    this->letra = letra;
}

// Se obtiene la info de aminoacidos.
list<Aminoacido> Cadena::get_aminoacido(){
    return this->aminoacido;
}

// Se agrega los aminoacidos a la cadena
void Cadena::add_aminoacido(Aminoacido aminoacido){
    this->aminoacido.push_back(aminoacido);
}

