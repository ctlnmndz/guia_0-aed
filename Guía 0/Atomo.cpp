#include <iostream>
using namespace std;
#include "Atomo.h"
#include "Coordenada.h"

//Constructor
Atomo::Atomo(int numero_a, string nombre_a){
    this->numero_a = numero_a;
    this->nombre_a = nombre_a;
    this->coordenada = Coordenada();
}
//Metodos get y set
int Atomo::get_numero_a(){
    return this->numero_a;
}
void Atomo::set_numero_a(int numero_a){
    this->numero_a = numero_a;
}

string Atomo::get_nombre_a(){
    return this->nombre_a;
}
void Atomo::set_nombre_a(string nombre_a){
    this->nombre_a = nombre_a;
}
// se obtiene info de coordenada.
Coordenada Atomo::get_coordenada(){
    return this->coordenada;
}
// Se agrega coordenada a atomo.
void Atomo::set_coordenada(Coordenada coordenada){
    this->coordenada = coordenada;
}


