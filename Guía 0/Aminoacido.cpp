#include <iostream>
#include <list>
using namespace std;
#include "Aminoacido.h"
#include "Atomo.h"

//Constructor
Aminoacido::Aminoacido(int numero_aa, string nombre_aa){
    this->numero_aa = numero_aa;
    this->nombre_aa = nombre_aa;
    list <string> atomo;
}
// Metodos get y set
int Aminoacido::get_numero_aa(){
    return this->numero_aa;
}

void Aminoacido::set_numero_aa(int numero_aa){
    this->numero_aa = numero_aa;
}

string Aminoacido::get_nombre_aa(){
    return this->nombre_aa;
}

void Aminoacido::set_nombre_aa(string nombre_aa){
    this->nombre_aa = nombre_aa;
}

// se obtiene la info de atomo.
list<Atomo> Aminoacido::get_atomo(){
    return this->atomo;
}
// Se agrega los atomos a los aminoacidos.
void Aminoacido::add_atomo(Atomo atomo){
    this->atomo.push_back(atomo);
}



