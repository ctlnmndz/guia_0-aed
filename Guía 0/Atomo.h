#include <iostream>
using namespace std;
#include "Coordenada.h"


#ifndef ATOMO_H
#define ATOMO_H

// Se crea la clase
class Atomo {
    private:
        int numero_a = 0;
        string nombre_a = "\0";
        Coordenada coordenada;
    public:
        Atomo(int numero_a, string nombre_a);

        //metodo get y set
        int get_numero_a();
        void set_numero_a(int numero_a);
        string get_nombre_a();
        void set_nombre_a(string nombre_a);
        Coordenada get_coordenada();
        void set_coordenada(Coordenada coordenda);
};
#endif
