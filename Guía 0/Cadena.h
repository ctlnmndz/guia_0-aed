#include <iostream>
#include <list>
using namespace std;
#include "Aminoacido.h"


#ifndef CADENA_H
#define CADENA_H

// se crea la clase.
class Cadena {
    private:
        string letra;
        list <Aminoacido> aminoacido;
    public:
        Cadena(string letra);

        //metodo get y set
        string get_letra();
        void set_letra(string letra);
        list<Aminoacido> get_aminoacido();
        void add_aminoacido(Aminoacido aminoacido);
};
#endif
