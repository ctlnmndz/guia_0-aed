#include <iostream>
#include <list>
using namespace std;
#include "Cadena.h"


#ifndef PROTEINA_H
#define PROTEINA_H

// Se crea la clase
class Proteina {
    private:
        string nombre = "\0";
        string id = "\0";
        list <Cadena> cadena;
    public:
        Proteina(string nombre, string id);
        //metodos get y set
        string get_id();
        void set_id(string id);
       
        string get_nombre();
        void set_nombre(string nombre);
        
        list <Cadena> get_cadena();
        void add_cadena(Cadena cadena);
};
#endif
