#include <iostream>
using namespace std;

#ifndef COORDENADAS_H
#define COORDENADAS_H

// Se crea la clase
class Coordenada {
    private:
        float x = 0;
        float y = 0;
        float z = 0;

    public:
        Coordenada();
        Coordenada(float x, float y, float z);

        // Metodo get y set
        float get_x();
        void set_x(float x);
        
        float get_y();
        void set_y(float y);
        
        float get_z();
        void set_z(float z);

};
#endif
