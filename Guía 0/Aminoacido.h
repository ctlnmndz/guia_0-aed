#include <iostream>
#include <list>
using namespace std;
#include "Atomo.h"


#ifndef AMINOACIDO_H
#define AMINOACIDO_H

// Se crea la clase
class Aminoacido {
    private:
        int numero_aa;
        string nombre_aa;
        list <Atomo> atomo;
    public:
        Aminoacido(int numero_aa, string nombre_aa);

        //metodo get y set
        int get_numero_aa();
        void set_numero_aa(int numero_aa);
        string get_nombre_aa();
        void set_nombre_aa(string nombre_aa);
        list<Atomo> get_atomo();
        void add_atomo(Atomo atomo);

};
#endif
