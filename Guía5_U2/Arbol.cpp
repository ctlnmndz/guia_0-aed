#include <iostream>
#include <unistd.h>
#include <fstream>
using namespace std;
#include <string>
#include "Programa.h"
#include "Arbol.h"

#define TRUE 1
#define FALSE 0


Arbol::Arbol(){

}


// Función que inserta el nodo
void Arbol::InsercionBalanceado(Nodo *&nodocabeza, int *BO, string infor) {
	Nodo *nodo = NULL;
	Nodo *nodo1 = NULL;
	Nodo *nodo2 = NULL;
	nodo = nodocabeza;
	
	// si el nodo es distinto de 0
	if (nodo != NULL) {
		// si es menor que la raiz se agrega en el lado izquierdo
		if (infor.compare(nodo->info) == -1) {
			InsercionBalanceado((nodo->izq), BO, infor);
			if(*BO == TRUE) {
			
				switch (nodo->FE) {
					case 1:
						nodo->FE = 0;
						*BO = FALSE;
						break;
						
					case 0:
						nodo->FE = -1;
						break;
					case -1:
						/* reestructuración del árbol */
						nodo1 = nodo->izq;
						if (nodo1->FE <= 0) {
							nodo->izq = nodo1->der;
							nodo1->der = nodo;
							nodo->FE = 0;
							nodo = nodo1;
						}
						else {
							/* Rotacion ID */
							nodo2 = nodo1->der;
							nodo->izq = nodo2->der;
							nodo2->der = nodo;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;
           						
							if(nodo2->FE == -1)
								nodo->FE = 1;
								
							else
								nodo->FE = 0;
									
							if (nodo2->FE == 1)
								nodo1->FE = -1;
								
							else
 								nodo1->FE = 0;
									
							nodo = nodo2;
              			}
						nodo->FE = 0;
						*BO = FALSE;
						break;
				}
			}
		}
		else {
			// si es mayor que la raiz se agregará en el lado derecho
			if (infor.compare(nodo->info) == 1){
				InsercionBalanceado((nodo->der), BO, infor);
				if (*BO == TRUE) {
					switch (nodo->FE) {
						case -1:
							nodo->FE = 0;
							*BO = FALSE;
							break;
						case 0:
							nodo->FE = 1;
							break;
							
						case 1:
							nodo1 = nodo->der;
							if (nodo1->FE >= 0) { 
								/* Rotacion DD */
								nodo->der = nodo1->izq;
								nodo1->izq = nodo;
								nodo->FE = 0;
								nodo = nodo1;
							} 
							else {
								/* Rotacion DI */
								nodo2 = nodo1->izq;
								nodo->der = nodo2->izq;
								nodo2->izq = nodo;
								nodo1->izq = nodo2->der;
								nodo2->der = nodo1;
								if (nodo2->FE == 1)
									nodo->FE = -1;
								
								else
									nodo->FE = 0;
								
								if (nodo2->FE == -1)
									nodo1->FE = 1;
								
								else
									nodo1->FE = 0;
								
								nodo = nodo2;
							}
							nodo->FE = 0;
							BO = FALSE;
							break;
					}
				}
			}
			else {					
				cout << "El Nodo ya se encuentra en el árbol" << endl;
			}
		}
	}
	else{
		// se crea nodo
		nodo = new Nodo;
		nodo->info = infor;
		nodo->izq = NULL;
		nodo->der = NULL;
		nodo->FE = 0;
		*BO = TRUE;
	}
	nodocabeza = nodo;
}


// Función que busca nodo
void Arbol::Busqueda(Nodo *nodo, string infor){
	// si el nodo es distinto de 0
	if(nodo != NULL){
		// si es menor se busca en el lado izquierdo
		if(infor < nodo->info){
			Busqueda(nodo->izq, infor);
		}
		else{
			// si es mayor se busca en el lado derecho
			if(infor > nodo->info) {
				Busqueda(nodo->der, infor);
			}
			// si se encuentra imprime que SÍ
			else{
				cout << "El Nodo SI se encuentra en el árbol" << endl;
			}
		}
	}
	// si no lo encuentra imprime que NO
	else{
		cout << "El Nodo NO se encuentra en el árbol" << endl;
	}
}


void Arbol::Restructura1(Nodo **nodocabeza, int *BO) {
	Nodo *nodo, *nodo1, *nodo2;
	nodo = *nodocabeza;
	
	if (*BO == TRUE) {
		switch (nodo->FE) {
			case -1:
				nodo->FE = 0;
				break;
			case 0:
				nodo->FE = 1;
				*BO = FALSE;
				break;
			case 1:
				/* reestructuracion del árbol */
				nodo1 = nodo->der;
				if (nodo1->FE >= 0) {
					/* rotacion DD */
					nodo->der = nodo1->izq;
					nodo1->izq = nodo;
					switch (nodo1->FE) {
						case 0:
							nodo->FE = 1;
							nodo1->FE = -1;
							*BO = FALSE;
							break;
						case 1:
							nodo->FE = 0;
							nodo1->FE = 0;
							*BO = FALSE;
							break;
					}
					nodo = nodo1;
				} 
				else {
					/* Rotacion DI */
					nodo2 = nodo1->izq;
					nodo->der = nodo2->izq;
					nodo2->izq = nodo;
					nodo1->izq = nodo2->der;
					nodo2->der = nodo1;
					
					if (nodo2->FE == 1)
						nodo->FE = -1;
					else
						nodo->FE = 0;
					if (nodo2->FE == -1)
						nodo1->FE = 1;
					else
						nodo1->FE = 0;
					
					nodo = nodo2;
					nodo2->FE = 0;
					
				}
				break;
			}
		}
	*nodocabeza = nodo;
}


void Arbol::Restructura2(Nodo **nodocabeza, int *BO) {
	Nodo *nodo, *nodo1, *nodo2;
	nodo = *nodocabeza;
	
	if (*BO == TRUE){
		switch (nodo->FE){
			case 1:
				nodo->FE = 0;
				break;
			case 0:
				nodo->FE = -1;
				*BO = FALSE;
				break;
			case -1:
				/* reestructuracion del árbol */
				nodo1 = nodo->izq;
				if (nodo1->FE<=0) {
					/* rotacion II */
					nodo->izq = nodo1->der;
					nodo1->der = nodo;
					switch (nodo1->FE){
						case 0:
							nodo->FE = -1;
							nodo1->FE = 1;
							*BO = FALSE;
							break;
						case -1:
							nodo->FE = 0;
							nodo1->FE = 0;
							*BO = FALSE;
							break;
					}
					nodo = nodo1;
				} 
				else {
					/* Rotacion ID */
					nodo2 = nodo1->der;
					nodo->izq = nodo2->der;
					nodo2->der = nodo;
					nodo1->der = nodo2->izq;
					nodo2->izq = nodo1;
					
					if (nodo2->FE == -1)
						nodo->FE = 1;
					else
						nodo->FE = 0;
					if (nodo2->FE == 1)
						nodo1->FE = -1;
					else
						nodo1->FE = 0;
					
					nodo = nodo2;
					nodo2->FE = 0;
				}
				break;
		}
	}
	*nodocabeza = nodo;
}


// Función para eliminar nodo
void Arbol::EliminacionBalanceado(Nodo **nodocabeza, int *BO, string infor) {
	Nodo *nodo, *otro;
	nodo = *nodocabeza;
	
	// Si la raiz no esta vacía se puede eliminar
	if (nodo != NULL) {
		// Se identifica el subárbol al que pertence el numero
		if (infor.compare(nodo->info) <= -1){
			// Recursividad para eliminar número
			EliminacionBalanceado(&(nodo->izq), BO, infor);
			Restructura1(&nodo, BO);
		} 
		else {
			// Si el número es mayor
			if (infor.compare(nodo->info) >= 1){
				// Recursividad para eliminar número
				EliminacionBalanceado(&(nodo->der), BO, infor);
				Restructura2(&nodo, BO);
			}
			// Si es un nodo se va al procedimiento eliminar nodo
			else {
				otro = nodo;
				// Subárbol derecho vacio
				if (otro->der == NULL) {
					nodo = otro->izq;
					*BO = TRUE;
				} 
				else {
					// Subárbol izquierdo vacio
					if (otro->izq==NULL) {
						nodo = otro->der;
						*BO = TRUE;
					} 
					else {
						// Llama la función borrar
						Borra(&(otro->izq), &otro, BO);
						Restructura1(&nodo, BO);
						// Liberar memoria
						free(otro);
					}
				}
			}
		}
	}
	// EL NODO NO EXISTE
	else {
		cout << "El nodo NO se encuentra en el árbol" << endl;
	}
	*nodocabeza = nodo;
}


// Función que borra el nodo
void Arbol::Borra(Nodo **aux1, Nodo **otro1, int *BO) {
	Nodo *aux, *otro;
	aux = *aux1;
	otro = *otro1;

	// Si el nodo se encuentra en el lado derecho
	if (aux->der != NULL) {
		Borra(&(aux->der), &otro, BO);
		Restructura2(&aux, BO);
	}
	// si el nodo existe en el lado izquierdo
	else{
		otro->info = aux->info;
		aux = aux->izq;
		*BO = TRUE;
	}
	// Se cambia el valor de las temporales
	*aux1 = aux;
	*otro1 = otro;
}


// Función que crea el archivo txt
void Arbol::recorrer(Nodo *p, ofstream &fp){
	if(p != NULL){
		if (p->izq != NULL) {
			fp << "\"" <<  p->info << "\"" << "->" << "\"" << p->izq->info << "\"" << "[label=" << p->izq->FE << "];" << endl;
	
		} 
		else{
			fp << "\"" << p->info << "i\"" << " [shape=point];" << endl;
			fp << "\"" << p->info << "\"" << "->" << "\"" << p->info << "i\"" << ";" << endl;
		}
		if (p->der != NULL) {
			fp << "\"" << p->info << "\"" << "->" << "\"" << p->der->info << "\"" << "[label=" << p->der->FE << "];" << endl;
		} 
		else{
			fp << "\"" << p->info << "d\"" << " [shape=point];" << endl;
			fp << "\"" << p->info << "\"" << "->" << "\"" << p->info << "d\"" << ";" << endl;

		}
		recorrer(p->izq, fp);
		recorrer(p->der, fp);
	}
}


// función que crea la imagen
void Arbol::GenerarGrafo(Nodo *p){
	ofstream archivo;
	// Se abre/crea el archivo grafo.txt, a partir de este se generará el grafo.
	archivo.open("grafo.txt");
	// Se escribe dentro del archivo grafo.txt "digraph G { ".
	archivo << "digraph G {" << endl;
	// Se pueden cambiar los colores que representarán a los nodos, para el ejemplo el color será verde.
	archivo << "node [style=filled fillcolor=cyan];" << endl;
	// Llamado a la función recursiva que genera el archivo de texto para creación del grafo.
	archivo << "nullraiz [shape=point];" << endl;
	archivo << "nullraiz->" << "\"" << p->info << "\"" << " ";
 	archivo << "[label=" << p->FE << "];" << endl;

	recorrer(p, archivo);
	// Se termina de escribir dentro del archivo numeros.txt.
	archivo << "}";
	archivo.close();
	
	// Genera el grafo
	system("dot -Tpng -ografo.png grafo.txt &");
	// Visualizar el grafo
	system("eog grafo.png &");
}
