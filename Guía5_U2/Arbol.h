#include <iostream>
#include <unistd.h>
#include <fstream>
using namespace std;
#include <string>

#ifndef ARBOL_H
#define ARBOL_H

class Arbol{
	private:
	
	
	public:
		//constructor
		Arbol();
		// Ingresa nodos al árbol
		void InsercionBalanceado(Nodo *&nodocabeza, int *BO, string infor);
		// Busca nodos en el árbol
		void Busqueda(Nodo *nodo, string infor);
		// Restructura 
		void Restructura1(Nodo **nodocabeza, int *BO);
		void Restructura2(Nodo **nodocabeza, int *BO);
		// Elimina nodo
		void Borra(Nodo **aux1, Nodo **otro1, int *BO);
		void EliminacionBalanceado(Nodo **nodocabeza, int *BO , string infor);
		// Recorre el árbol
		void recorrer(Nodo *p, ofstream &fp);
		// Crea el grafo
		void GenerarGrafo(Nodo *raiz);
};
#endif
