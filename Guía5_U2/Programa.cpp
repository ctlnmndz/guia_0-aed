#include <unistd.h>
#include <fstream>
#include <iostream>
using namespace std;
#include <string>

#include "Programa.h"
#include "Arbol.h"


#define TRUE 1
#define FALSE 0


int main(){
	// Variables
	string elemento;
	string elemento2;
	int opcion=0;
	string linea;
	int inicio = FALSE;
	
	// Crea el árbol
	Arbol *Nuevo_Arbol = new Arbol();
	//abre archivo
	ifstream archivo("pdbs.txt",ios::in);

	Nodo *raiz = NULL;
	
	cout << "MENÚ" << endl;
	// Ciclo para repetir las opciones del menú
	while (opcion!=7){
		cout << "***********************" << endl;
		cout << "Ingrese una opción:" << endl;
		cout << "1.- Ingresar PDBs" << endl;
		cout << "2.- Ingresar Nodo" << endl;
		cout << "3.- Buscar nodo" << endl;
		cout << "4.- Eliminar nodo" << endl;
		cout << "5.- Modificar nodo" << endl;
		cout << "6.- Mostrar grafo" << endl;
		cout << "7.- Salir" << endl;
		cout << "***********************" << endl;
	    cin >> opcion;
	   
	    // Ingresa PDBs al arbol y muestra el grafo
		if (opcion==1){
			// recorre archivo 
			while(getline(archivo, linea)){
				Nuevo_Arbol->InsercionBalanceado(raiz, &inicio, linea);
			}
			// crea grafo
			Nuevo_Arbol->GenerarGrafo(raiz);

		}
		// Agrega nodos al árbol
		else if(opcion == 2){
			cout << "Ingrese el nodo a agregar:" << endl;
			cin >> elemento;
      		Nuevo_Arbol->InsercionBalanceado(raiz, &inicio, elemento);


		}
		// Busca nodos
		else if(opcion == 3){
			cout << "Igrese nodo a buscar:" << endl;
			cin >> elemento;
      		Nuevo_Arbol->Busqueda(raiz, elemento);


		}
		// Elimina nodos
		else if(opcion == 4){
			cout << "Ingresa el nodo a eliminar" << endl;
			cin >> elemento;
      		Nuevo_Arbol->EliminacionBalanceado(&raiz, &inicio, elemento);


		}
		// Modifica nodos
		else if(opcion == 5){
			// Se pide nodo a modificar
			cout << "Ingrese el nodo que desea modificar: ";
			cin >> elemento;
			// Se elimina
			Nuevo_Arbol->EliminacionBalanceado(&raiz, &inicio, elemento);
			// se pide nodo a reemplazar
			cout << "Ingrese el nuevo nodo: ";
			cin >> elemento2;
			// se agrega el nuevo nodo
			Nuevo_Arbol->InsercionBalanceado(raiz, &inicio, elemento2);

		}
		// Crea y muestra el grafo
		else if(opcion == 6){
			cout << "Grafo:" << endl;
			Nuevo_Arbol->GenerarGrafo(raiz);

		}
		// Salir del programa
		else if(opcion == 7){
			cout << "Ha salido del programa, xao xoxo" << endl;
		}
		// Si no es válida la opción
		else{
			 cout << "Opción no válida" << endl;		
		}
	}
	return 0;
}
