#include <iostream>
using namespace std;

#ifndef ORDENA_H
#define ORDENA_H

class Ordena {
	private:

	public:
		// Constructor 
		Ordena();
		// metodo que ordena con el metodo seleccion
		void ordena_seleccion(int arreglo[], int numero);
		// metodo que ordena con el metodo quicksort
		void ordena_quicksort(int arreglo[], int numero);
		// metodo que imprime el arreglo ordenado 
		void imprimir_vector(int arreglo[], int numero);
};
#endif
