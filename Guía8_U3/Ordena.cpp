#include <iostream>
#include <fstream>
#include <string>
using namespace std;
#include "Ordena.h"

#define TRUE 0
#define FALSE 1

// Constructor
Ordena::Ordena(){

}


// funcion que ordena por el metodo selecicon
void Ordena::ordena_seleccion(int arreglo[], int numero){
	int i, menor, k, j;
	
	// ciclo para recorrer el arreglo
	for (i=0; i<=numero-2; i++) {
		// se toma el primer valor
		menor = arreglo[i];
		k = i;
    	// ciclo recorre el arreglo en una posicion siguiente al anterior
		for (j=i+1; j<=numero-1; j++) {
			// si el siguiente valor del arreglo es menor se guarda en la variable menor el menor
			if (arreglo[j] < menor) {
				menor = arreglo[j];
				k = j;
			}
		}
    	// se cambian las variables y se guarda el numero menor en la primera posicion
		arreglo[k] = arreglo[i];
		arreglo[i] = menor;
	}
}


// funcion que ordena con el metodo quicksort
void Ordena::ordena_quicksort(int arreglo[], int numero) {
	int tope, ini, fin, pos;
	int pilamenor[1000000];
	int pilamayor[1000000];
	int izq, der, aux, band;
	
	// se dan los valores iniciales
	tope = 0;
	pilamenor[tope] = 0;
	pilamayor[tope] = numero-1;
	
	// ciclo que se repite cuando el tope es mayor o igual a 0
	while (tope >= 0) {
		// se dan los valores inicial y final
		ini = pilamenor[tope];
		fin = pilamayor[tope];
		tope = tope - 1;
		
		// reduce
		izq = ini;
		der = fin;
		pos = ini;
		band = TRUE;
		
		
		while (band == TRUE) {
			// si el primer numero es menor igual a la siguiente y es diferente 
			while ((arreglo[pos] <= arreglo[der]) && (pos != der))
				// se le disminuye 1
				der = der - 1;
        	// si la posicion es igual a el de la derecha se terminia
			if (pos == der) {
				band = FALSE;
			// si no se cambian las varibles para evaluar al siguiente
			} else {
				aux = arreglo[pos];
				arreglo[pos] = arreglo[der];
				arreglo[der] = aux;
				pos = der;
        		
        		// si la posicion actual es mayor al primero y diferente
				while ((arreglo[pos] >= arreglo[izq]) && (pos != izq))
					// aumenta en 1
					izq = izq + 1;
					
				// si la posicion es igual al anterior se termina
				if (pos == izq) {
					band = FALSE;
				
				// si no se evalua el siguiente numero del vector
				} else {
					aux = arreglo[pos];
					arreglo[pos] = arreglo[izq];
					arreglo[izq] = aux;
					pos = izq;
				}
			}
		}
		// si cambian las varibles
		if (ini < (pos - 1)) {
			tope = tope + 1;
			pilamenor[tope] = ini;
			pilamayor[tope] = pos - 1;
		}
    	// se cambian las variables
		if (fin > (pos + 1)) {
			tope = tope + 1;
			pilamenor[tope] = pos + 1;
			pilamayor[tope] = fin;
		}
	}
}


// funcion que imprime el arreglo
void Ordena::imprimir_vector(int arreglo[], int numero){
	for(int i=0; i<numero; i++){
		cout << "arreglo["<< i << "]=" << arreglo[i] << " ";
	}
	cout << endl;
}
