#include <cstring>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sstream>
using namespace std;
#include "Ordena.h"


// Función que llena y manda a ordenar los arreglos.
void llenar_ordenar(int numero, string opcion){
	// se cran los arreglos
	int *arreglo1 = new int[numero];
	int *arreglo2 = new int[numero];
	int elemento;
	
	// se llama la función
	Ordena ordena;
	
	// Se llena la los arreglos aleatoriamente
	for(int i=0; i < numero; i++){
		elemento = (rand ()% 5000) + 1;
		arreglo1[i] = elemento;
		arreglo2[i] = elemento;
		// si se pide mostrar los datos del vector, se muestran
		if (opcion == "s" || opcion == "S"){
			cout << "arreglo[" << i << "]=" << arreglo1[i] << " ";
		}
	}
	
	// Se toma el tiempo de cuanto se demora el metodo selection
	int t1 = clock();
	ordena.ordena_seleccion(arreglo1, numero);
	int t2 = clock();
	int secs1 = (double)(t2 - t1) / CLOCKS_PER_SEC;
	// Se toma el tiempo de cuanto se demora el metodo quicksort
	int t3 = clock();
	ordena.ordena_quicksort(arreglo2, numero);
	int t4 = clock();
	int secs2 = (double)(t4 - t3) / CLOCKS_PER_SEC;
	cout << endl;
	
	// Se imprime la info de tiempo de los metodos en ordenar
	cout<< "-----------------------------------------" << endl;
	cout << "Método            |Tiempo" << endl;
	cout<< "-----------------------------------------" << endl;
	cout << "Seleccion         |" << secs1*1000.0 << " milisegundos" << endl;
	cout << "Quicksort         |" << secs2*1000.0 << " milisegundos" << endl;
	cout << "-----------------------------------------" << endl;
	
	// Si se pide mostrar los datos del vector, se imprimen en orden
	if (opcion == "s" || opcion == "S"){
		cout << "Seleccion: " << endl;
		ordena.imprimir_vector(arreglo1, numero);
		cout << "Quicksort: " << endl;
		ordena.imprimir_vector(arreglo2, numero);
		cout << endl;
	}
}


// función principal 
int main(){
	int numero;
	string opcion;
	int op = 0;

	// ciclo para pedir número válido
	while(op==0){
		
		cout << "Ingrese la cantidad de numeros: " << endl;
		cin >> numero;
		// si el numero esta dentro del rango se llama la funcion para llenar y ordenar.
		if(numero > 0 && numero <= 1000000){
			cout << "Ingrese s si quiere ver los vectores y n si no:" << endl;
			cin >> opcion;
			llenar_ordenar(numero, opcion);
			// se cambia la variable op para termianr el ciclo
			op=1;
			}
		// si no es valido se repite 
		else{
			cout << "Número invalido, vuelva a ingresar el numero" << endl;
			op=0;
		}
	}
	
	return 0;
}

