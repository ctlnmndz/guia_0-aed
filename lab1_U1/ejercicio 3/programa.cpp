#include <iostream>
using namespace std;
#include "Clientes.h"

int main(){
	int cantidad = 0;
    string nombre;
    int telefono;
    int saldo;
    bool moroso;
    
    cout << "Ingrese la cantidad de clientes: " << endl;
    cin >> cantidad;
    
    string clientes[cantidad][4];
    // Se llama la clase
    Clientes c = Clientes();
   
   // ciclo que pide datos e imprime
    for(int i =0; i<cantidad; i++){
        for(int j = 0; j<4; j++){
            if (j==0){
                cout << "Ingrese Nombre del cliente: " << endl;
                cin >> nombre;
                c.set_nombre(nombre);
                clientes[i][j] = c.get_nombre();
            }
            else if (j==1){
                cout << "Ingrese telefono del cliente: " << endl;
                cin >> telefono;
                c.set_telefono(telefono);
                clientes[i][j] = c.get_telefono();
            }
            else if (j==2){
                cout << "Ingrese saldo del cliente: " << endl;
                cin >> saldo;
                c.set_saldo(saldo);
                clientes[i][j] = c.get_saldo();
            }
            else if (j==3){
                cout << "Ingrese 1 si el cliente es moroso y si no es ingrese 2: " << endl;
                cin >> moroso;
                c.set_moroso(moroso);
                clientes[i][j] = c.get_moroso();
            }
        }
        // Se imprime la informacon del cliente
        cout << "****Cliente****" << endl;
        cout << "El nombre del cliente es: " << c.get_nombre() << endl;
        cout << "El telefono del cliente es: " << c.get_telefono() << endl;
        cout << "El saldo del cliente es: " << c.get_saldo() << endl;
        if(c.get_moroso() == 1){
            cout << "EL cliente es moroso" << endl;
        }
        else if(c.get_moroso() == 2){
            cout << "El cliento no es moroso" << endl;
        }
    }
	return 0;
}

