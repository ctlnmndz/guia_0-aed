using namespace std;

#ifndef PERSONA_H
#define PERSONA_H

// Clase persona
class Clientes {
    private:
        string nombre = "\0";
        int telefono = 0;
        int saldo = 0;
        bool moroso = 0;
    public:
        // constructor
        Clientes();

        // Metodos get and set
        string get_nombre();
        void set_nombre(string nombre);
        int get_telefono();
        void set_telefono(int telefono);
        int get_saldo();
        void set_saldo(int saldo);
        bool get_moroso();
        void set_moroso(bool moroso);
};
#endif
