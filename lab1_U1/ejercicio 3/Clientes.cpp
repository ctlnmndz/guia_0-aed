#include <iostream>
using namespace std;
#include "Clientes.h"

// constructor
Clientes::Clientes(){
    string nombre = "\0";
    int telefono = 0;
    int saldo = 0;
    bool moroso = 0;
}

// Metodo get and set para devolver y obtener las varibles del cliente.
string Clientes::get_nombre(){
    return this->nombre;
}
void Clientes::set_nombre(string nombre){
    this->nombre = nombre;
}


void Clientes::set_telefono(int telefono){
    this->telefono = telefono;
}
int Clientes::get_telefono(){
    return this->telefono;
}


int Clientes::get_saldo(){
    return this->saldo;
}
void Clientes::set_saldo(int saldo){
    this->saldo = saldo;
}


bool Clientes::get_moroso(){
    return this->moroso;
}

void Clientes::set_moroso(bool moroso){
    this->moroso = moroso;
}
