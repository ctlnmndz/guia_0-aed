#include <iostream>
#include <cctype>
using namespace std;
#include "Frase.h"

// funcion para ver cuantas mayusculas y minusculas hay en la frase
void minusculas_mayusculas(string analizar){
    int minusculas = 0;
    int mayusculas = 0;
    
    // Se analiza cada palabra de la frase
    for(int i=0; i< analizar.size(); i++){
            // si es mayuscula el contador mayusculas aumenta 1
            if(isupper(analizar[i])){
                mayusculas = mayusculas + 1;
            }
            // si es minuscula el contador minusculas aumenta 1
            else if(islower(analizar[i])){
                minusculas = minusculas + 1;
           }
        }  
        // se imprimen las cantidades de mayusculsa y minusculas
        cout << "Cantidad de minusculas: " << minusculas << endl;
        cout << "Cantidad de mayusculas: " << mayusculas << endl;

}


int main(){
    
    int cantidad =0;
    string frase;
    string analizar;
    
    cout << "Ingrese la cantidad de frases: " << endl;
    cin >> cantidad;
    
    string frases[cantidad];
    
    for(int i=0; i<cantidad; i++){
        
        cout << "Ingrese la frase: " << endl;
        getline(cin,frase);
        // se llama la clase
        Frase f = Frase();
        f.set_frase(frase);
        
        cout << "Frase: " << f.get_frase() << endl; 
        frases[i]= f.get_frase();
        /* se manda la frase a la funcion minuscula_mayuscula para 
        evaluar cuantas minusculas y mayusculas tiene*/
        analizar = f.get_frase();
        minusculas_mayusculas(analizar);
    }
    return 0;
}

