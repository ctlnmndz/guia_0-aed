using namespace std;

#ifndef FRASE_H
#define FRASE_H

// Se crea la clase 
class Frase {

    private:
        string frase = "\0";

    public:
        // Constructor
        Frase();

        // Metodos get and set
        string get_frase();
        void set_frase(string frase);
};
#endif
