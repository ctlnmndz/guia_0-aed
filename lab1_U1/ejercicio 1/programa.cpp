#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
#include "Cuadrado.h"

int main(){
    srand(time(NULL));

    int dim = 0;
    int numero=0;
    int suma = 0;
    
    cout << "Ingrese la cantidad de numeros a ingresar al arreglo: " << endl;
    cin >> dim;

    int arreglo[dim];
    
    cout << "Los numeros ingresados al arreglo son: " << endl;
    // ciclo que llena el arreglo con los cuadrados de los numero aleatorio
    for(int i=0; i<dim; i++){
        // se genera un numero aleatorio entre 1 y 10
        numero = rand()%(10-1);
        cout << numero << endl;

        // se llama la clase
        Cuadrado c = Cuadrado();
        c.set_numero(numero);

        // se llena el arreglo con los numero aleatorios generados.
        arreglo[i] = c.get_numero();

        // se trae el cuadrado del numero
        c.set_cuadrado();
        suma = c.get_cuadrado() + suma;
    }
    cout << "La suma de los cuadrados de los numeros ingresados al arreglo es: " << suma << endl;


    return 0;
}