using namespace std;

#ifndef CUADRADO_H
#define CUADRADO_H

// Se crea la clase 
class Cuadrado {

    private:
        int numero = 0;
        int cuadrado = 0;

    public:
        // Constructor
        Cuadrado();

        // Metodos get and set
        int get_numero();
        void set_numero(int numero);
        int get_cuadrado();
        void set_cuadrado();
};
#endif
