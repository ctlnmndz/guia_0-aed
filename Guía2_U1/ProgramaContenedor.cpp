#include <iostream>
using namespace std;
#include "Pila.h"


//imprime puerto
void imprimir_puerto(int largo, int alto, Pila puerto[]){
	cout << "*************PUERTO*************" << endl;
	// Recorre la matriz e imprime
  	for(int i=alto-1; i>=0; i--){
    	for(int j=0; j<largo; j++){
      	cout << "|" << puerto[j].get_pila(i) << "|";
    	}
    	cout << "\n";
  	}
  	cout << "********************************" << endl;
}


// Funcion para mover contenedores
void mover_contenedores(int largo, int alto, Pila puerto[]){
	int fila;
	int columna;
	string contenedor;
	int pila;
	int contador=0;
    
    cout << "Ingrese el nombre de contenedor a mover: " << endl;
    cin >> contenedor;
	cout << contenedor;
	// Recorre la matriz 
	for(int i=0; i<largo; i++){
    	for(int j=0; j<alto; j++){
			// Si encuentra el contenedor que se va a mover guarda su posición y suma 1 el contador
      		if(puerto[i].get_pila(j)==contenedor){
        		fila = i;
        		columna = j;
    			contador++;
			}
    	}
	}
	// si la encuentra se mueve
	if(contador!=0){
		cout << "Ingrese la fila donde desea mover el contenedor: ";
    	cin >> pila;
   		//se mueve el contedor a la pila que desea
 		puerto[pila-1].mover(contenedor);
   		// con la posicion se elimina el contenedor a mover
		puerto[fila].pop(columna);
		imprimir_puerto(largo, alto, puerto);
	}
	else{
		cout << "El contenedor ingresado no se encontró" << endl;
	}
}


// Función para eliminar contenedores
void eliminar_contenedor(int largo, int alto, Pila puerto[]){
	int contenedores_acumulados;
	int fila;
	int columna;
	string contenedor;
	int contador =0;
	
    cout << "Ingres nombre de contenedor a eliminar: ";
    cin >> contenedor;
    
    // recorre la matriz
    for(int i=0; i<largo; i++){
    	for(int j=0; j<alto; j++){
			
			// Si encuentra el contenedor se guarda la posicion
      		if(puerto[i].get_pila(j)==contenedor){
				
				// Se obtiene cuantos contenedores hay arriba del que se pide
        	 	contenedores_acumulados = puerto[i].get_tope() - j;
        		fila = i;
        		columna = j;
        		contador++;
        		
        		/*si los contenedores es igual a 1 se puede eliminar,
        		ya que solo se contó el que se esta pidiendo a eliminar, 
        		no hay contenedores arriba de el*/
        		if(contenedores_acumulados==1){
					puerto[fila].pop(columna);

				}
				/* Si  el numero es mayor se deben mover los contenedores 
				que se encuentran arriba para poder eliminar*/
				else{
					cout << "Debe mover contenedores para poder eliminarlo" << endl;
				}
  
			}
    	}
	}
	if(contador==0){
		cout << "El contenedor a eliminar no se ha encontrado en el puerto" << endl;
	}
}


// Función menú
void menu(int largo, int alto, Pila puerto[]){
	int opcion = 10;
	int marcador;
	// ciclo para pedir el menú las veces necesarias hasta que opriman el 0.
	while(opcion != 0){
		cout << "**************MENÚ**************" << endl;
		cout << "Ingrese una opción: " << endl;
		cout << "1. Ingresar un contenedor" << endl;
		cout << "2. Remover un contenedor" << endl;
		cout << "3. Ver puerto" << endl;
		cout << "4. Mover contenedores" << endl;
		cout << "0. Salir" << endl;
		cout << "********************************" << endl;
		cin >> opcion;
		
		// se llama el metodo de añadir contenedores en la clase pila
		if(opcion == 1){
			cout << "Ingrese en la pila que agregará el contenedor: "<< endl;
			cin >> marcador;
       		puerto[marcador-1].push();
		}
		// Se llama el metodo de eliminar contenedores en la clase pila
		else if(opcion == 2){
        	eliminar_contenedor(largo, alto, puerto);
		}
		// Se llama la función para imprimir el puerto
		else if(opcion == 3){
			imprimir_puerto(largo, alto, puerto);
		}
		// Se llama la función para mover contenedores
		else if(opcion == 4){
			mover_contenedores(largo, alto, puerto);
		}
		// Se sale del programa
		else if(opcion == 0){
			cout << "Ha salido del progama" << endl;
		}

		else{
			cout << "Opción invalida" << endl;
		}
	}
}
int main(){
  int largo;
  int alto;
  
  cout << "Ingrese el largo del puerto: ";
  cin >> largo;
  cout << "Ingrese el alto del puerto: ";
  cin >> alto;

  // Se llama la clase y se crea la matriz para crear el puerto
  Pila *puerto = new Pila[largo];
  for(int i=0; i<largo; i++){
    puerto[i].llenar(alto);
  }

  menu(largo, alto, puerto);

  return 0;
}
