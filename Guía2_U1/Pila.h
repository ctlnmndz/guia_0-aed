using namespace std;

#ifndef PILA_H
#define PILA_H

// Se crea la clase pila.
class Pila{
    private:

        int max = 0;
        string *pilas = NULL;
        int tope = 0;
        bool band;
        string dato;

    public:
        // Constructor de la clase.
        Pila();
        Pila(int max);
        
        int get_tope();
        string get_pila(int j);
        void llenar(int max);
        void mover(string contenedor);
        void Pila_llena();
        void push();
        void Pila_vacia();
        void pop(int columna);
};
#endif
