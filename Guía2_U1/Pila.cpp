#include <iostream>
using namespace std;
#include "Pila.h"

//Contructores de la clase
Pila::Pila(){

  int max = max;
  int tope = tope;
  string *pilas = NULL;
  bool band;
  this-> band = band;
  string dato = dato;
}


// metodo para obtener el puerto
string Pila::get_pila(int j){
	if(this->pilas[j] == "\0"){
		//Se le da un valor de *vacío*
		return "*vacío*";
	}
	return this->pilas[j];
}


//metodo para crear el puerto
void Pila::llenar(int max){
	this->max = max;
	//crea matriz
	this->pilas = new string[max];
}


// evalúa si la pila está llena.
void Pila::Pila_llena(){
    // si el tope es igual a la cantidad maxima, la pila esta llena.
    if(this-> tope  ==  this-> max){
        this-> band = true;
    }
    // si no no esta llena.
    else{
        this-> band = false;
    }
}


//metodo para añadir un contenedor
void Pila::push(){
    // Vemos si la pila esta llena.
    Pila_llena();
    // Si esta llena no se agrega el dato.
    if(this-> band == true){
		cout << "Desbordamiento, Pila llena" << endl;
    // si no esta llena se añade el dato al arreglo pila.   
    }else{
        //Se pide el dato.
        cout << "Ingrese el nombre del contenedor que desa agregar: " << endl;
        cout << "Ejemplo: 1/empresa" << endl;
        cin >> dato;
       
        // Se añade.
        this-> pilas[this->tope] = dato;
        // El tope aumenta en 1.
        this -> tope = this-> tope +1;
        cout << "¡Dato agregado con exito!" << endl;
    }
    cout << endl;
}


// evalúa si la pila esta vacía.
void Pila::Pila_vacia(){
	// si el tope es igual a 0, la pila esta vacía.
    if(this-> tope == 0){
        this-> band = true;
 
    }else{
      this-> band = false;
    }
}


// Metodo que elimina contenedor.
void Pila::pop(int columna){
	// Se ve si la pila esta vacía.
    Pila_vacia();
    // Si esta vacía no se elimina nada.
    if(this-> band == true){
		cout << "Subdesbordamiento, Pila vacı́a" << endl;
	// Si no esta llena se elimina el dato añadido.
    }else{
		this -> pilas[columna] = "*vacío*";
		cout << "¡Dato elimidado con exito!" << endl;     
		this -> tope = tope-1;   
    }
}


// metodo para mover una pila
void Pila::mover(string contenedor){
    // Vemos si la pila esta llena.
    Pila_llena();
    // Si esta llena no se agrega el dato.
    if(this-> band == true){
		cout << "Desbordamiento, Pila llena" << endl;
    // si no esta llena se añade el dato al arreglo pila.   
    }else{
        // Se añade.
        this-> pilas[this->tope] = contenedor;
        // El tope aumenta en 1.
        this -> tope = this-> tope +1;
        cout << "¡Dato Movido con exito!" << endl;
    }
    cout << endl;
}


// metodo para obtener el tope
int Pila::get_tope(){
 	return this->tope;
}
