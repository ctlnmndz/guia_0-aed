#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo, recibe una instancia de la clase Numero. */
        void crear (Numero *numero);
        /* ordena la lista de menor a mayor*/
        void ordenar();
        /* imprime la lista. */
        void imprimir ();
};
#endif
