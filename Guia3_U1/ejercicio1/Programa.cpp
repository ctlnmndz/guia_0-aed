#include <iostream>
using namespace std;

/* clases */
#include "Programa.h"
#include "Numero.h"
#include "Lista.h"


class Programa {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Programa() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};

// Menu del programa
void menu(Lista *lista){
	int opcion=0;
	int numero;
	cout << "LISTA ENLAZADA ORDENADA" << endl;
	
	// ciclo para repetir las opciones
	while (opcion!=2){
		cout << "***********************" << endl;
		cout << "Ingrese una opción:" << endl;
		cout << "1.- Ingresar un número" << endl;
		cout << "2.- Salir del programa" << endl;
		cout << "***********************" << endl;
	    cin >> opcion;
	    
	    // Se ingresa numero
		if (opcion==1){
			cout << "Ingrese el número a agreagar a la lista:" << endl;
			cin >> numero;
			// Se llama la clase numero 
			Numero *numero1 = new Numero(numero);
			// se añade el numero a la lista
    		lista->crear(numero1);
    		// se ordena la lista
    		lista->ordenar();
    		// imprime la lista
    		lista->imprimir();
		}
		else if(opcion==2){
			cout << "Ha salido del programa, xao xoxo" << endl;
		}
		else{
			 cout << "Opción no válida" << endl;		
		}
	}
}
/* función principal. */
int main (void) {
	
    Programa e = Programa();
    Lista *lista = e.get_lista();
    menu(lista);
    
    return 0;
}
