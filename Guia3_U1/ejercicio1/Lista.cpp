#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear (Numero *numero) {
    Nodo *tmp;

    /* crea un nodo . */
    tmp = new Nodo;
    /* asigna la instancia del numero. */
    tmp->numero = numero;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista. */
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}
// ordena la lista (metodo burbuja)
void Lista::ordenar(){
	// Se crea una variable temporal
	Nodo *tmp = this->raiz;
    int aux;
    // Si hay elementos en la lista la recorre
    while(tmp != NULL){
		// Se crea una segunda temporal
        Nodo *tmp2 = tmp-> sig;
        while(tmp2 !=NULL){
			// Si el primer numero es mayor que el segundo se cambian
        	if(tmp->numero->get_numero() > tmp2->numero->get_numero()){
				// se guarda el numero de la temporal 2 en la variable auxiliar
            	aux = tmp2->numero->get_numero();
            	// La temporal 2 se le da el valor de la temporal 1
            	tmp2->numero->set_numero(tmp-> numero->get_numero());
            	/*a la temporal 1 se le da el valor de la variable 
            	 auxiliar que antes era el valor de la temporal 2*/
            	tmp->numero->set_numero(aux);
        	}
        	// la temporal 2 toma el siguiente numero
        	tmp2 = tmp2 -> sig;
    	}
    	// la temporal 1 toma el siguiente numero
    	tmp = tmp-> sig;
    }
}

void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << "***Estado actual de la lista***" << endl;
    while (tmp != NULL) {
        cout << tmp->numero->get_numero() << endl;
        tmp = tmp->sig;
    }
}
