#include <iostream>
using namespace std;

#ifndef PERSONA_H
#define PERSONA_H

class Numero {
    private:
        int numero = 0;

    public:
        /* constructor */
        Numero(int numero);
        
        /* métodos set and get */
        void set_numero(int numero);
        int get_numero();
};
#endif
