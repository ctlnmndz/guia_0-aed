#include <iostream>
using namespace std;

/* clases */
#include "Programa.h"
#include "Lista.h"


class Programa {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Programa() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};

// funcion menu
void menu(Lista *lista1, Lista *lista2, Lista *lista3){
	int opcion=0;
	int numero;
	cout << "LISTA ENLAZADA SIMPLE DE ENTEROS" << endl;
	// Ciclo para repetir las opciones del menu
	while (opcion!=4){
		cout << "***********************" << endl;
		cout << "Ingrese una opción:" << endl;
		cout << "1.- Ingresar un número a la lista 1" << endl;
		cout << "2.- Ingresar un número a la lista 2" << endl;
		cout << "3.- Ver estado de las listas" << endl;
		cout << "4.- Salir del programa" << endl;
		cout << "***********************" << endl;
	    cin >> opcion;
	    // Se añade el numero a la lista 1 y la lista 3 a la vez
		if (opcion==1){
			cout << "Ingrese el número a agreagar a la lista 1:" << endl;
			cin >> numero;
			// Se añade el numero a la listas
    		lista1->crear(numero);
    		lista3->crear(numero);
    		// se ordenan las listas
    		lista1->ordenar();
    		lista3->ordenar();
		}
		// Se añade el numero a la lista 2 y la lista 3 a la vez
		else if(opcion == 2){
			cout << "Ingrese el número a agreagar a la lista 2:" << endl;
			cin >> numero;
			// Se añade el numero a la listas
    		lista2->crear(numero);
    		lista3->crear(numero);
    		// se ordenan las listas
    		lista2->ordenar();
    		lista3->ordenar();
		}
		// opcion para imprimir las listas
		else if(opcion == 3){
			/* se imprimen las 3 listas*/
			cout << "    LISTA 1    " << endl;
    		lista1->imprimir();
    		cout << "    LISTA 2    " << endl;
    		lista2->imprimir();
    		cout << "    LISTA 3    " << endl;
    		lista3->imprimir();
		}
		// opcion para salir del programa
		else if(opcion==4){
			cout << "Ha salido del programa, xao xoxo" << endl;
		}
		else{
			 cout << "Opción no válida" << endl;		
		}
	}
}
/* función principal. */
int main (void) {
	// Se crean 3 listas
    Programa programa = Programa();
    Lista *lista1 = programa.get_lista();
    Programa programaa = Programa();
    Lista *lista2 = programaa.get_lista();
    Programa programaaa = Programa();
    Lista *lista3 = programaaa.get_lista();
    menu(lista1, lista2 , lista3);
    
    return 0;
}
