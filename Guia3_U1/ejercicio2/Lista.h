#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo. */
        void crear (int numero);
        /* ordena la lista de menor a mayor*/
        void ordenar();
        /* llena la lista con los numeros que faltan*/
        void rellenar(Lista *lista);
        /* imprime la lista. */
        void imprimir ();
};
#endif
