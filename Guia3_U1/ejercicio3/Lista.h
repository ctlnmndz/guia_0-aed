#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo, recibe una instancia de la clase Persona. */
        void crear (int numero);
        /* ordena la lista*/
        void ordenar();
        /* llena la lista con los numeros faltantes*/
        void rellenar(Lista *lista);
        /* imprime la lista. */
        void imprimir ();
};
#endif
