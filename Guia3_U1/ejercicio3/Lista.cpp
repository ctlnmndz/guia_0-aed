#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear(int numero){
	/* crea un nodo . */
    Nodo *tmp = new Nodo;

     /* crea un nodo . */
    tmp -> numero = numero;
	/* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if(this->raiz == NULL){
    	this-> raiz = tmp;
    	this->ultimo = this->raiz;
    	tmp -> sig = NULL;

    }
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista. */
    else{
        this->ultimo-> sig = tmp;
        this->ultimo = tmp;
    }
    ordenar();
}
void Lista::rellenar(Lista *lista){
	// se crean las varibles temporales
	Nodo *tmp = this->raiz;
	Nodo *tmp2 = tmp-> sig;
	int numero;
	int contador = 0;
	
	if(tmp2 != NULL){
		while(tmp2 != NULL){
   			// si no son seguidos los numeros se añadiran
       		if(tmp2->numero != tmp->numero + 1){
				// se obtiene el numero siguiente de la variable temporal
				numero = tmp-> numero +1;
				// se añade el numero
    			lista->crear(numero);
    			// suma contadr
    			contador++;
       			}
       		// se evaluan los siguientes nodos de la lista
			tmp = tmp2;
   			tmp2 = tmp2->sig;
		}
		// si el contador es distinto de 0 es porque se añadieron los numeros faltantes
		if (contador!=0){
			cout << "     Lista llenada      " << endl;
		}
		// si es 0 es porque los numeros ya estan seguidos (1,2,3)
		else{
			cout << " Nada que llenar " <<endl;
		}
   	}
   	// sino es porque en la lista hay 1 o 0 numeros
   	else{
		cout << "Datos insufiecientes para llenar la lista" << endl;
	}
}

void Lista::ordenar(){
	// Se crea una variable temporal
	Nodo *tmp = this->raiz;
    int aux;
    // Si hay elementos en la lista la recorre
    while(tmp != NULL){
		// Se crea una segunda temporal
        Nodo *tmp2 = tmp-> sig;
        while(tmp2 !=NULL){
			// Si el primer numero es mayor que el segundo se cambian
        	if(tmp->numero> tmp2->numero){
				// se guarda el numero de la temporal 2 en la variable auxiliar
            	aux = tmp2->numero;
            	// La temporal 2 se le da el valor de la temporal 1
            	tmp2->numero= tmp-> numero;
            	/*a la temporal 1 se le da el valor de la variable 
            	 auxiliar que antes era el valor de la temporal 2*/
            	tmp-> numero= aux;
        	}
        	// la temporal 2 toma el siguiente numero
        	tmp2 = tmp2 -> sig;
    	}
    	// la temporal 1 toma el siguiente numero
    	tmp = tmp-> sig;
    }
}
void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << "***Estado actual de la lista***" << endl;
    while (tmp != NULL) {
        cout << tmp->numero << endl;
        tmp = tmp->sig;
    }
}
