#include <iostream>
using namespace std;

/* clases */
#include "Programa.h"
#include "Lista.h"


class Programa {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Programa() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};

// funcion menu
void menu(Lista *lista){
	int opcion=0;
	int numero;
	cout << "LISTA ENLAZADA SIMPLE DE ENTEROS" << endl;
	// ciclo que repite las opciones del menu
	while (opcion!=3){
		cout << "***********************" << endl;
		cout << "Ingrese una opción:" << endl;
		cout << "1.- Ingresar un número" << endl;
		cout << "2.- Rellenar lista" << endl;
		cout << "3.- Salir del programa" << endl;
		cout << "***********************" << endl;
	    cin >> opcion;
	    // se agregan numeros a la lista
		if (opcion==1){
			cout << "Ingrese el número a agreagar a la lista:" << endl;
			cin >> numero;
			// se añade el numero a la lista
    		lista->crear(numero);
    		// ordena la lista
    		lista->ordenar();
    		// imprime la lista
    		lista->imprimir();
		}
		// se rellena la lista
		else if(opcion == 2){
			// rellena la lista
			lista->rellenar(lista);
			// imprime la lista
			lista->imprimir();
		}
		else if(opcion==3){
			cout << "Ha salido del programa, xao xoxo" << endl;
		}
		else{
			 cout << "Opción no válida" << endl;		
		}
	}
}
/* función principal. */
int main (void) {
	// se crea la lista
    Programa programa = Programa();
    Lista *lista = programa.get_lista();
    menu(lista);
    
    return 0;
}
