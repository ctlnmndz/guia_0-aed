#include <fstream>
#include <iostream>
#include <string>
using namespace std;
#include "Hash.h"


// Función que ingresa los numeros al arreglo
void ingresar(int *arreglo, int numero){
	string opcion="avanza";
	int indice;
	int contador=0;
	
	Hash *clase = new Hash();
	// se saca el indice de la posicion del numero
	indice = clase->hash(numero, 20);
	
	// se evalua si el numero a ingresar ya esta en el arreglo
	for(int i=0; i<20; i++){
		if(arreglo[i]==numero){
			contador=contador+1;
		}
	}
	// si no esta en el arreglo, se puede agregar
	if(contador== 0){
		// se imprime el indice 
		cout << "Indice posición: " << indice <<endl;
		cout << endl;
		// si la posicion esta vacia, se arega
		if(arreglo[indice]=='\0'){
			arreglo[indice] = numero;
		}
		// si no, hay colision
		else{
			cout << "Colisión" << endl;
			cout << endl;
			// se pide una opcion de resolver la colision
			while(opcion == "avanza"){
				cout << "Eliga un tipo de ordenamiento, ingrese la letra correspondiente : "<<endl;
				cout << "------------------------------------ " << endl;
				cout << "L.-Prueba lineal" << endl;
				cout << "C.-Prueba cuadratica" << endl;
				cout << "D.-Doble dirección Hash" << endl;
				cout << "E.-Encadenamiento" << endl;
				cout << "------------------------------------ " << endl;
				cin >> opcion;
				
				// opcion de solucion de colision tipo lineal
				if(opcion=="l" || opcion=="L"){
					// llama la funcion
					clase->ingreso_lineal(arreglo, 20, numero, indice);
				}
				// opcion de solucion de colision tipo cuadratica
				else if(opcion=="c" || opcion=="C"){
					// llama la funcion
					clase->ingreso_cuadratica(arreglo, 20, numero, indice);
				}
				// opcion de solucion de colision tipo doble hash
				else if(opcion=="d" || opcion=="D"){
					// llama la funcion
					clase->ingreso_doble_hash(arreglo, 20, numero, indice);
				}
				// opcion de solucion de colision tipo encadenamiento
				else if(opcion=="e" || opcion =="E"){
					cout << "aun no ordena por encadenamiento" << endl;
					cout << endl;
				
				}
				// cuando apreta una tecla que no es parte del menú
				else{
					cout << "Elección inválida" << endl;
					opcion = "avanza";
					cout << endl;
				}
			}
		}
	}
	// si no, no se agrega
	else{
		cout << "El número ya se encuentra en el arreglo, ingrese otro número" << endl;
	}
	// se imprime el arreglo
	cout << "------------------ ------------- ----------------- ------------------ " << endl;
	for (int i=0; i<20; i ++){
		cout << "[" << arreglo[i] << "] ";
	}
	cout<< endl;
	cout << "------------------ ------------- ----------------- ------------------ " << endl;
	cout << endl;
}


// función principal 
int main(){
	int arreglo[20];
	int opcion;
	int op = 0;
	int numero;
	int cont=0;
	
	Hash *clase = new Hash();
	
	// se llena el arreglo
	for(int i = 0; i <20; i++){
		arreglo[i]= '\0';
	}

	// ciclo menú
	while(op==0){
		// opciones del menú
		cout << endl;
		cout << "------------------Menú------------------ " << endl;
		cout << "1.- Ingresar número " << endl;
		cout << "2.- Buscar número " << endl;
		cout << "3.- Salir del programa" << endl;
		cout << "---------------------------------------- " << endl;
		cin >> opcion;
		cout << endl;
		
		// opcion para ingresar numeros al arreglo
		if(opcion == 1){
			// suma 1 al contador al entrar a la opcion 
			cont=cont+1;
			// si el contador es menor a 20 se ingresa el numero al arreglo
			if(cont<20){
				cout << "Ingrese el numero a añadir al arreglo: " << endl;
				cin >> numero;
				ingresar(arreglo, numero);
			}
			// sino el arreglo esta lleno
			else{
				cout << "Arreglo lleno, no se puede agregar más números" << endl;
			}
			
		}
		// opcion para buscar numero
		else if(opcion == 2){
			// se pide el numero a buscar y se llama la funcion de busqueda
			cout << "Ingrese el numero a buscar en el arreglo: " << endl;
			cin >> numero;
			clase->busqueda(arreglo, 20, numero);
		}
		//opcion para salir del programa
		else if(opcion == 3){
			cout << "Ha salido del programa, xoxo" << endl;
			op = 1;
			
		}
		// si presiona cualquier tecla que no este en el menú, se repite.
		else{
			cout << "Elección inválida" << endl;
		}
	}
	
	return 0;
}

