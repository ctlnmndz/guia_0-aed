#include <iostream>
#include <fstream>
using namespace std;
#include "Hash.h"

// Constructor
Hash::Hash(){

}


// funcion hash
int Hash::hash(int numero, int tamano) {
	numero = (numero%(tamano-1)) + 1;
	return numero;
}


// funcion de ingreso lineal
void Hash::ingreso_lineal(int *arreglo, int tamano, int numero, int indice){
	int nuevo_indice;
	
    // se suma en una el indice 
	nuevo_indice = indice + 1;

    //si en el nuevo indice esta ocupada se vuelve a sumar uno para ver en la siguiente posicion
	while (arreglo[nuevo_indice] != '\0'){
		nuevo_indice = nuevo_indice + 1;

		/*si se termina evaluar el arreglo se vuelve al inicio para 
			ver si queda un espacio vacio a la izquierda*/
		if (nuevo_indice == (tamano + 1)){
			nuevo_indice = 0;
		}
	}

    // si en el nuevo indice esta vacio, se añade el numero
	if (arreglo[nuevo_indice] == '\0')	{
		arreglo[nuevo_indice] = numero;
		// se imprime el proceso ocurrido
		cout << "El numero " << numero << " de la posicion " << indice << " se desplazada a la posición " << nuevo_indice<< endl;
	}
}


// función de ingreso cuadratica
void Hash::ingreso_cuadratica(int *arreglo, int tamano, int numero, int indice){
	int nuevo_indice;
	int cuadrado=0;

	// se calcula el nuevo indice sumandole el cuadrado
	nuevo_indice = indice + (cuadrado*cuadrado);
    
    //si la posicion del nuevo indice esta ocupada, vulve a calcularlo
	while(arreglo[nuevo_indice] != '\0') {
		cuadrado=cuadrado+1;
		nuevo_indice = indice + (cuadrado*cuadrado);

		/* si se termina de revisar el arreglo hacia la derecha se comienza 
			del inicio para ver si en la izquierda queda un espacio*/
		if (nuevo_indice > tamano) {
			cuadrado = 0;
			nuevo_indice = 0;
			indice = 0;
		}
	}
    // Si en el nuevo indice esta vacio, se añade el numero 
	if (arreglo[nuevo_indice] == '\0') {
		arreglo[nuevo_indice] = numero;
		// se imprime el proceso ocurrido
		cout << "El numero " << numero << " de la posicion " << indice << " se desplazada a la posición " << nuevo_indice<< endl;
	}
}


// funcion de ingreso doble hash
void Hash::ingreso_doble_hash(int *arreglo, int tamano, int numero, int indice){
	int nuevo_indice;

	//Se vuelve a sacar hash al numero
	nuevo_indice = ((indice + 1)%tamano - 1) + 1;
   
    // si la posicion no esta vacio vuelve a sacr el hash
	while (arreglo[nuevo_indice] != '\0'){
		nuevo_indice= ((nuevo_indice + 1)%tamano - 1) + 1;
	}
	
    // Si el nuevo indice esta vacio se añada al arreglo
	if (arreglo[nuevo_indice] == '\0') {
		// se imprime el nuevo indice
		cout << "Nuevo indice es: " << nuevo_indice <<endl;
		arreglo[nuevo_indice] = numero;
		// se imprime el proceso ocurrido
		cout << "El numero " << numero << " de la posicion " << indice << " se desplazada a la posición " << nuevo_indice<< endl;
	}
}


// funcion que busca el numero en el arreglo
void Hash::busqueda(int *arreglo, int tamano, int numero){
	int indice;
	int contador =0;
	// saca el indice de posicion
	indice = hash(numero, 20);
	
	//recorre el arreglo
	for(int i=0; i<tamano; i++){
		// si en el arreglo esta el numero aumenta el contador
		if(arreglo[i]==numero){
			contador = contador +1;
			// se imprime en que indice se encuentra
			cout << "El número se encuentra en el arreglo en la posición " << i << endl;
			// si en la posicion que se encuentra el numero es el mismo que en el indice, no hubo colision
			if(i==indice){
				cout << "No hubo colisión" << endl;
			}
			// si son diferentes es porque hubo colision
			else{
				cout << "Sí hubo colisión" << endl;
				// se imprime la posicion donde deberia ir y a donde se desplazo
				cout << "De la posición " << indice << " se desplazo a la posición " << i << endl;
			}
			
		}
	}
	// si el contador no aumenta, es decir, es 0, el numero no se encuentra en el arreglo
	if(contador == 0){
		cout << "El número no se encuentra en el arreglo" << endl;
	}
}
