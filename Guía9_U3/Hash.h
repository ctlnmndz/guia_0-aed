#include <iostream>
using namespace std;

#ifndef Hash_H
#define Hash_H

// Clase
class Hash{

	private:


	public:
		//Constructor
		Hash();

		// metodos para sacar el indice de posicion
		int hash(int clave, int largo);
		// metodos para ingresar los numeros
		void ingreso_lineal(int *arreglo, int tamano, int numero, int indice);
		void ingreso_cuadratica(int *arreglo, int tamano, int numero, int inidce);
		void ingreso_doble_hash(int *arreglo, int tamano, int numero, int indice);
		// medoto para para buscar el numero
		void busqueda(int *arreglo, int tamano, int numero);

};
#endif
